package com.isw.svcord03.integration.partylife.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.isw.svcord03.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord03.sdk.integration.partylife.partylife.model.LoginResponse;
import com.isw.svcord03.sdk.integration.partylife.partylife.provider.LoginApiPartylife;
import com.isw.svcord03.sdk.integration.partylife.service.RetrieveLoginBase;
import com.isw.svcord03.sdk.integration.facade.IntegrationEntityBuilder;

@Service
public class RetrieveLogin extends RetrieveLoginBase {

  private static final Logger log = LoggerFactory.getLogger(RetrieveLogin.class);

  @Autowired
  LoginApiPartylife partyLife;

  public RetrieveLogin(IntegrationEntityBuilder entityBuilder ) { 
    super(entityBuilder );
  }
  
  @NewSpan
  @Override
  public com.isw.svcord03.sdk.integration.partylife.entity.RetrieveLoginOutput execute(com.isw.svcord03.sdk.integration.partylife.entity.RetrieveLoginInput retrieveLoginInput)  {

    log.info("RetrieveLogin.execute()");
    // TODO: Add your service implementation logic
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.set("accept", "application/json");
    httpHeaders.set("Content-Type", "application/json");

    LoginResponse loginResponse = partyLife.getUserLogin(retrieveLoginInput.getId(), httpHeaders).getBody();

    RetrieveLoginOutput loginOuput = this.entityBuilder.getPartylife().getRetrieveLoginOutput().build();
    loginOuput.setResult(loginResponse.getResult().getValue());

    return loginOuput;

  }

}
