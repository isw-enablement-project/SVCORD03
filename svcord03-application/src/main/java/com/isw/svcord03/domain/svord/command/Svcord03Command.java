package com.isw.svcord03.domain.svord.command;


import org.springframework.stereotype.Service;
import com.isw.svcord03.sdk.domain.svord.command.Svcord03CommandBase;
import com.isw.svcord03.sdk.domain.svord.entity.CreateServicingOrderProducerInput;
import com.isw.svcord03.sdk.domain.svord.entity.Svcord03;
import com.isw.svcord03.sdk.domain.svord.entity.Svcord03Entity;
import com.isw.svcord03.sdk.domain.svord.entity.ThirdPartyReferenceEntity;

import com.isw.svcord03.sdk.domain.svord.type.ServicingOrderType;
import com.isw.svcord03.sdk.domain.svord.type.ServicingOrderWorkProduct;
import com.isw.svcord03.sdk.domain.svord.type.ServicingOrderWorkResult;
import com.isw.svcord03.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord03.sdk.domain.facade.Repository;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class Svcord03Command extends Svcord03CommandBase {

  private static final Logger log = LoggerFactory.getLogger(Svcord03Command.class);

  public Svcord03Command(DomainEntityBuilder entityBuilder,  Repository repo ) {
    super(entityBuilder,  repo );
  }

  
    
    @Override
    public com.isw.svcord03.sdk.domain.svord.entity.Svcord03 createServicingOrderProducer(CreateServicingOrderProducerInput createServicingOrderProducerInput)  {
    

      log.info("Svcord03Command.createServicingOrderProducer()");
      // TODO: Add your command implementation logic
      Svcord03Entity servicingOrderProcedure = this.entityBuilder.getSvord().getSvcord03().build();
      servicingOrderProcedure.setCustomerReference(createServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessStartDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderType(ServicingOrderType.PAYMENT_CASH_WITHDRAWALS);
      servicingOrderProcedure.setServicingOrderWorkDescription("CASH WITHDRAWALS");
      servicingOrderProcedure.setServicingOrderWorkProduct(ServicingOrderWorkProduct.PAYMENT);
      servicingOrderProcedure.setServicingOrderWorkResult(ServicingOrderWorkResult.PROCESSING);
   
      ThirdPartyReferenceEntity thirdPartyReference = new ThirdPartyReferenceEntity();
      thirdPartyReference.setId("test1");
      thirdPartyReference.setPassword("password");
      servicingOrderProcedure.setThirdPartyReference(thirdPartyReference);

      return repo.getSvord().getSvcord03().save(servicingOrderProcedure);

    }
  
    
    @Override
    public void updateServicingOrderProducer(com.isw.svcord03.sdk.domain.svord.entity.Svcord03 instance, com.isw.svcord03.sdk.domain.svord.entity.UpdateServicingOrderProducerInput updateServicingOrderProducerInput)  { 

      Svcord03Entity servicingOrderProcedure = this.repo.getSvord().getSvcord03().getReferenceById(Long.parseLong(updateServicingOrderProducerInput.getUpdateID()));
      servicingOrderProcedure.setCustomerReference(updateServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessEndDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderWorkResult(updateServicingOrderProducerInput.getServicingOrderWorkResult());
      servicingOrderProcedure.setThirdPartyReference(updateServicingOrderProducerInput.getThirdPartyReference());

      log.info(updateServicingOrderProducerInput.getUpdateID().toString());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAccountNumber());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAmount());

      this.repo.getSvord().getSvcord03().save(servicingOrderProcedure);

      
    }



    //@Override
   // public Svcord03 createServicingOrderProducer(CreateServicingOrderProducerInput createServicingOrderProducerInput) {
      // TODO Auto-generated method stub
     // throw new UnsupportedOperationException("Unimplemented method 'createServicingOrderProducer'");
   // }
  
}
