package com.isw.svcord03.domain.svord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord03.sdk.domain.svord.entity.CreateServicingOrderProducerInput;
import com.isw.svcord03.sdk.domain.svord.entity.CustomerReferenceEntity;
import com.isw.svcord03.sdk.domain.svord.entity.Svcord03;
import com.isw.svcord03.sdk.domain.svord.entity.UpdateServicingOrderProducerInput;
import com.isw.svcord03.sdk.domain.svord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord03.sdk.domain.svord.service.WithdrawalDomainServiceBase;
import com.isw.svcord03.sdk.domain.svord.type.ServicingOrderWorkResult;
import com.isw.svcord03.sdk.integration.facade.IntegrationEntityBuilder;
import com.isw.svcord03.sdk.integration.partylife.entity.RetrieveLoginInput;
import com.isw.svcord03.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord03.sdk.integration.paymord.entity.PaymentOrderInput;
import com.isw.svcord03.sdk.integration.paymord.entity.PaymentOrderOutput;
import com.isw.svcord03.domain.svord.command.Svcord03Command;
import com.isw.svcord03.integration.partylife.service.RetrieveLogin;
import com.isw.svcord03.integration.paymord.service.PaymentOrder;
import com.isw.svcord03.sdk.api.v1.model.CustomerReference;
import com.isw.svcord03.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord03.sdk.domain.facade.Repository;

@Service
public class WithdrawalDomainService extends WithdrawalDomainServiceBase {

  private static final Logger log = LoggerFactory.getLogger(WithdrawalDomainService.class);

  @Autowired
  Svcord03Command servicingOrderProcedureCommand;

  @Autowired
  PaymentOrder paymentOrder;

  @Autowired
  RetrieveLogin retrieveLogin;

  @Autowired
  IntegrationEntityBuilder integrationEntityBuilder;

  public WithdrawalDomainService(DomainEntityBuilder entityBuilder,  Repository repo) {
  
    super(entityBuilder,  repo);
    
  }
  
  @NewSpan
  @Override
  public com.isw.svcord03.sdk.domain.svord.entity.WithdrawalDomainServiceOutput execute(com.isw.svcord03.sdk.domain.svord.entity.WithdrawalDomainServiceInput withdrawalDomainServiceInput)  {

    log.info("WithdrawalDomainService.execute()");
    // TODO: Add your service implementation logic

    RetrieveLoginInput retrieveInput = integrationEntityBuilder
    .getPartylife()
    .getRetrieveLoginInput()
    .setId("testId")
    .build();

    RetrieveLoginOutput retrieveLoginOutput = retrieveLogin.execute(retrieveInput);

    if(retrieveLoginOutput.getResult()!="SUCCESS"){

      return null;

    }

    CustomerReferenceEntity customerReference = this.entityBuilder.getSvord().getCustomerReference().build();
    customerReference.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    customerReference.setAmount(withdrawalDomainServiceInput.getAmount());

    CreateServicingOrderProducerInput createInput = this.entityBuilder.getSvord().getCreateServicingOrderProducerInput().build();
    createInput.setCustomerReference(customerReference);
    Svcord03 createOutput = servicingOrderProcedureCommand.createServicingOrderProducer(createInput);

    
    PaymentOrderInput paymentOrderInput = integrationEntityBuilder.getPaymord().getPaymentOrderInput().build();

    paymentOrderInput.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    paymentOrderInput.setAmount(withdrawalDomainServiceInput.getAmount());
    paymentOrderInput.setExternalId("SVCORD03");
    paymentOrderInput.setExternalSerive("SVCORD03");
    paymentOrderInput.setPaymentType("CASH_WITHDRAWAL");

    PaymentOrderOutput paymentOrderOutput = paymentOrder.execute(paymentOrderInput);

    //paymentOrder.execute(paymentOrderInput);

    UpdateServicingOrderProducerInput updateServicingOrderProducerInput = this.entityBuilder.getSvord()
    .getUpdateServicingOrderProducerInput().build();
    updateServicingOrderProducerInput.setUpdateID(createOutput.getId().toString());
    updateServicingOrderProducerInput
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()));

    servicingOrderProcedureCommand.updateServicingOrderProducer(createOutput, updateServicingOrderProducerInput);

    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = this.entityBuilder.getSvord().getWithdrawalDomainServiceOutput()
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()))
    .setTrasactionId(paymentOrderOutput.getTransactionId())
    .build();


    


    //this.entityBuilder.getSvord().getCreateServicingOrderProducerInput().setCustomerReference(null);
    //retrieveLogin.execute(retrieveInput);
    return withdrawalDomainServiceOutput;
  }

}
